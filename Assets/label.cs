using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class label : MonoBehaviour
{
    // Start is called before the first frame update
    RaycastHit raycastHit;
    [SerializeField] Camera camera;
    void Awake()
    {
        camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) || Input.GetMouseButtonDown(0))
        {
            //Ray raycast = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            Ray raycast = camera.ScreenPointToRay(Input.mousePosition);
            
            if (Physics.Raycast(raycast, out raycastHit,1000))
            {
                Debug.Log("Something Hit");
               
                if (raycastHit.collider != null)
                {
                    if (raycastHit.collider.tag == "hardrive")
                    {
                        Debug.Log("Soccer Ball clicked");
                    }
                }
            }
        }
    }
}
