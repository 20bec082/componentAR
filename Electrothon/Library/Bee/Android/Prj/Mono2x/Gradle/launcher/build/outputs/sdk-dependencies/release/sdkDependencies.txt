# List of SDK dependencies of this app, this information is also included in an encrypted form in the APK.
# For more information visit: https://d.android.com/r/tools/dependency-metadata

library {
  maven_library {
    groupId: "androidx.core"
    artifactId: "core"
    version: "1.6.0"
  }
  digests {
    sha256: "\207]\276\310\210\311\033\005R\025u\375\030\f\300\210$\322\304\321+\020\260&\033O\235h%!N\361"
  }
}
library {
  maven_library {
    groupId: "androidx.annotation"
    artifactId: "annotation"
    version: "1.2.0"
  }
  digests {
    sha256: "\220)&+\335\316\021nm\002\276I\236J\375\272!\362L#\220\207\267k;W\327\351\213I\n6"
  }
}
library {
  maven_library {
    groupId: "androidx.annotation"
    artifactId: "annotation-experimental"
    version: "1.1.0"
  }
  digests {
    sha256: "\001W\336a\242\006@G\211j\005\200\200\363\375g\272W\255\232\224\205{?z66`$>?\220"
  }
}
library {
  maven_library {
    groupId: "androidx.lifecycle"
    artifactId: "lifecycle-runtime"
    version: "2.0.0"
  }
  digests {
    sha256: "\344\257\311\3466\030?o>\016\337\034\364a!\244\222\377\322\306s\a[\260\177U\307\251\235\324<\373"
  }
}
library {
  maven_library {
    groupId: "androidx.versionedparcelable"
    artifactId: "versionedparcelable"
    version: "1.1.1"
  }
  digests {
    sha256: "W\350\3312`\321\215[\220\a\311\356\323\306J\321Y\336\220\310`\236\277\307J4|\275QE5\244"
  }
}
library {
  maven_library {
    groupId: "androidx.collection"
    artifactId: "collection"
    version: "1.0.0"
  }
  digests {
    sha256: "\234\215\021{\\+\301 \241\315\376\270W\340[I[\026\303`\023W\003r\247\b\367\202~:\311\371"
  }
}
library {
  maven_library {
    groupId: "androidx.lifecycle"
    artifactId: "lifecycle-common"
    version: "2.0.0"
  }
  digests {
    sha256: "{\255z\030\210\004\255\352o\241\363]^\371\233p_ \275\223\354\255\336HG`\377\206\2655\376\374"
  }
}
library {
  maven_library {
    groupId: "androidx.arch.core"
    artifactId: "core-common"
    version: "2.0.0"
  }
  digests {
    sha256: "K\200\2637w\233Rnd\260\356\f\251\340\337C\270\b4M\024_\216\233\034B\2414\332\305z\330"
  }
}
library {
  maven_library {
    groupId: "com.google.ar"
    artifactId: "core"
    version: "1.22.0"
  }
  digests {
    sha256: "\016\251V\036:H\023v\257G\324T`\035\3747|\351\002\242\000\326dr\244\252V\224\223|\337u"
  }
}
library {
  maven_library {
    artifactId: "VuforiaEngine"
  }
  digests {
    sha256: "\352\324]n\323\232P\312\312%\302\237\311vXO\'\244\022\200\221\373WU\240\220\204C(\\T\207"
  }
}
library {
  digests {
    sha256: "\025Zz\224h\250\362\016RE\321\2118k?\235Z\375!\240\337q\246,\200\237\000\313\\\324L\n"
  }
}
library_dependencies {
  library_dep_index: 1
  library_dep_index: 2
  library_dep_index: 3
  library_dep_index: 4
  library_dep_index: 5
}
library_dependencies {
  library_index: 1
}
library_dependencies {
  library_index: 2
}
library_dependencies {
  library_index: 3
  library_dep_index: 6
  library_dep_index: 7
  library_dep_index: 1
}
library_dependencies {
  library_index: 6
  library_dep_index: 1
}
library_dependencies {
  library_index: 7
  library_dep_index: 1
}
library_dependencies {
  library_index: 4
  library_dep_index: 1
  library_dep_index: 5
}
library_dependencies {
  library_index: 5
  library_dep_index: 1
}
library_dependencies {
  library_index: 8
}
library_dependencies {
  library_index: 9
}
library_dependencies {
  library_index: 10
}
module_dependencies {
  module_name: "base"
  dependency_index: 10
}
