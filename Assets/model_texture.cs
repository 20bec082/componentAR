using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Baracuda;

public class model : MonoBehaviour
{
    public NNModel modelAsset;
    private Model m_RuntimeModel;
    filename = 'weights.onnx'
var modelAsset = ModelLoader.Load(filename);
    void Start()
    {
        m_RuntimeModel = ModelLoader.Load(modelAsset);
        m_Worker = WorkerFactory.CreateWorker(< WorkerFactory.Type >, m_RuntimeModel);
    }

    void Update()
    {
        Tensor input = new Tensor(batch, height, width, channels);
        m_Worker.Execute(input);
        Tensor O = m_Worker.PeekOutput("output_layer_name");
        input.Dispose();
    }
}
